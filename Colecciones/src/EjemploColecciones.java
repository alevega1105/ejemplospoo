import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class EjemploColecciones {

	public static void main(String[] args) {
		ArrayList<String> colores = new ArrayList<String>();
		colores.add("azul");
		colores.add("rojo");
		colores.add("verde");
		colores.add("blanco");
		System.out.println(colores.toArray());;
		
		for (String unColor : colores) {
			System.out.println(unColor);
		}
		colores.remove("blanco");
		System.out.println(colores);

		String[] colores2 = {"negro", "amarillo", "violeta", "plateado", "celeste", "magenta", "marron", "naranja", "rosado"};
		colores.addAll(Arrays.asList(colores2));
		System.out.println(colores);
		
		toUpperCase(colores);
		System.out.println(colores);
		
		//borrar sublista
		colores.subList(0, 4).clear();
		System.out.println(colores);
		
		//iterar hacia atras con un iterador
		ListIterator<String> iterator = colores.listIterator(colores.size());
		while (iterator.hasPrevious()) {
			System.out.println(iterator.previous());
		}
		
		//metodos de clase Collections
		//copy
		ArrayList<String> colores3 = new ArrayList<String>();
		String[] i = {"", "", "", "", "", "", "", ""};
		colores3.addAll(Arrays.asList(i));
		Collections.copy(colores3, colores);
		System.out.println(colores3);
		
		//fill
		Collections.fill(colores3, "azul");
		System.out.println(colores3);
		
		//sort
		Collections.sort(colores);
		System.out.println(colores);
		
		//shuffle
		Collections.shuffle(colores);
		System.out.println(colores);
		
		//binarySearch
		System.out.println(Collections.binarySearch(colores, "VIOLETA"));
		
		//frequency
		System.out.println(Collections.frequency(colores, "AMARILLO"));
		System.out.println(Collections.frequency(colores3, "azul"));
		
		//disjoint true si no tienen elementos en comun
		System.out.println("tienen elementos en comun? "+ Collections.disjoint(colores, colores3));
		
		//LinkedList
		LinkedList<String> llColores = new LinkedList<String>(colores);
		llColores.addLast("rojo");
		llColores.addFirst("azul");
		System.out.println(llColores);
		
		llColores.element(); //muestra pero no retira el elemento en la cabecera.
		llColores.poll(); //retira la cabecera.
		llColores.push("violeta"); //inserta un elemento por el frente de la lista
		llColores.pop(); //retira el primer elemento de la lista.
		
		//PriorityQueue
		PriorityQueue<String> queue = new PriorityQueue<String>(colores);
		System.out.println(queue);
		queue.offer("AZUL");
		System.out.println(queue);
		
		//HashSet
		HashSet<String> set = new HashSet<String>(colores);
		set.add("NARANJA");
		set.add("NARANJA");
		System.out.println(set);
		
		//HashMap
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		Scanner scanner = new Scanner(System.in);
		System.out.println("ingrese una oraci�n: ");
		String input = scanner.nextLine();
		String[] tokens = input.split(" ");
		
		for (String token : tokens) {
			String palabra = token.toLowerCase();
			if(hashMap.containsKey(palabra)) {
				int count = hashMap.get(palabra);
				hashMap.put(palabra, count+1);
			}else {
				hashMap.put(palabra, 1);
			}
		}
		System.out.println(hashMap);
	}
	
	private static void toUpperCase (List<String> lista) {
		ListIterator<String> iterator = lista.listIterator();
		while (iterator.hasNext()) {
			String color = iterator.next();
			iterator.set(color.toUpperCase());
		}
	}

}
