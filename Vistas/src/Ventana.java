import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Saludo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 349, 305);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(83, 129, 64, 14);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		lblNombre.setLabelFor(txtNombre);
		txtNombre.setBounds(169, 126, 96, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(83, 176, 64, 14);
		contentPane.add(lblApellido);
		
		txtApellido = new JTextField();
		lblApellido.setLabelFor(txtApellido);
		txtApellido.setBounds(169, 173, 96, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		JButton btnSaludo = new JButton("Saludar");
		btnSaludo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saludar();
			}
		});
		btnSaludo.setBounds(121, 232, 89, 23);
		contentPane.add(btnSaludo);
		
		JLabel lblImagen = new JLabel("");
		ImageIcon imageIcon = new ImageIcon(new ImageIcon("C:\\mario.png").getImage().getScaledInstance(120,96, Image.SCALE_DEFAULT));
		lblImagen.setIcon(imageIcon);
		lblImagen.setBounds(109, 22, 120, 96);
		contentPane.add(lblImagen);
		
		setLocationRelativeTo(null);
	}

	protected void saludar() {
	
		JOptionPane.showMessageDialog(this, "Bienvenido "+txtNombre.getText()+" "+txtApellido.getText());
		
	}

}
