
public class ResumenBasico {

	public static void main(String[] args) {
		// este es un comentario de linea simple
		
		/* este es un comentario de 
		 * multiples lineas
		 */
		
		/** y este es un comentario tipo JAVADOC
		 * 
		 */
		
		//TIPOS PRIMITIVOS
		
		int numEntero = 8;
		double numFlotante = 2.3;
		boolean booleano = true;
		char caracter = 'A';
		
		int casteo=  (int) 3.4;
		
		System.out.println("numero \n"+casteo);
		
		//OPERADORES LOGICOS
		
		if (numEntero == numFlotante) {
			System.out.println("los numeros son iguales");
		}else if(numEntero > numFlotante) {
			System.out.println("numero entero es mayor a flotante");
		}else {
			System.out.println("el entero es menor o igual al flotante");
		}
		/*
		 * igualdad: ==
		 * desigual o distinto: !=
		 * menor: <
		 * mayor: >
		 * menor o igual: <=
		 * mayor o igual: >=
		 */
		if(numEntero != numFlotante && numEntero >7) {
			System.out.println("el numero entero es distinto al flotante y es mayor a siete");
		}
		/*
		 * &: y
		 * |: o
		 * &&: y, no evaluando la segunda parte de la condicion si la primera es falsa
		 * ||: o, no evaluando la segunda parte de la condicion si la primera es verdadera
		 * ^: XOR
		 */
		
		System.out.println(numEntero >numFlotante? "entero> flotante": "entero <= flotante");
		
		int resultado = numEntero !=0 ? 100/numEntero : 0;
		
		switch (caracter) {
		case 'A': 
			 System.out.println("el valor de caracter es A");
			break;
		case 'B':
			System.out.println("el valor del caracter es B");
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + caracter);
		}
		//BUCLES
		 
		int[] numerosEnteros = {1, 3, 5, 2, 6, 8, 7};
		
		int i = 0;
		while(i< numerosEnteros.length) {
			System.out.println(numerosEnteros[i]);
			i ++;
		}
		int j = 6;
		do {
			numerosEnteros[j] = j;
			System.out.println("num en do while: "+numerosEnteros[j]);
			j--;
		} 
		while(j >=0);
		
		for (int k = 0; k < numerosEnteros.length; k++) {
			System.out.println("en for: "+ numerosEnteros[k]);
		}
		
		//operadores de asignacion compuestos
		
		i+=7; //igual a i = i+7
		i-=4;
		i *= 5;
		i /= 3;
		i %= 9;
		
		++i; //incrementa en uno y usa el valor 
		i++; // usa el valor y despues incrementa en uno. 
		
		

	}

}
