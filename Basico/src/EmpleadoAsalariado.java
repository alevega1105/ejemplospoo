
public class EmpleadoAsalariado extends Empleado {
	private Double salarioSemanal;

	public EmpleadoAsalariado(String nombre, String apellido, String dni,
			Double salarioSemanal) {
		super(nombre, apellido, dni);
		this.salarioSemanal = salarioSemanal;
	}
	@Override
	public Double ingresos() {
		return getSalarioSemanal();
	}

	@Override
	public String toString() {
		return "empleado asalariado: "+ super.toString();
	}

	public void setSalarioSemanal(Double salarioSemanal) {
		this.salarioSemanal = salarioSemanal;
	}

	public double getSalarioSemanal() {
		return salarioSemanal;
	}


} 