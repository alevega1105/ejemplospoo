
public class EmpleadoPorComision extends Empleado {

	private Double ventasBrutas; 
	private Double tarifaComision; 

	public EmpleadoPorComision(String nombre, String apellido, String dni, Double ventasBrutas, Double tarifaComision) {
		super(nombre, apellido, dni);
		this.ventasBrutas = ventasBrutas;
		this.tarifaComision = tarifaComision;
	}
	
	
	public Double ingresos() {
		return getTarifaComision() * getVentasBrutas();
	}

	@Override // indica que este m�todo sobrescribe el m�todo de una superclase
	public String toString() {
		return "empleado por comisi�n: "+super.toString()+ "\n ventas brutas: $"+getVentasBrutas()+"\n comisi�n: "+getTarifaComision() ;
	}
	
	
	//get y set

	public Double getTarifaComision() {
		return tarifaComision;
	}
	public Double getVentasBrutas() {
		return ventasBrutas;
	}
	public void setVentasBrutas(Double ventasBrutas) {
		this.ventasBrutas = ventasBrutas;
	}

	public void setTarifaComision(Double tarifaComision) {
		this.tarifaComision = tarifaComision;
	}




} 