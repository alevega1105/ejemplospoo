import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		EmpleadoAsalariado empleadoAsalariado = new EmpleadoAsalariado("Juan", "Sosa", "11111111", 80000.00);
		EmpleadoPorHoras empleadoPorHoras = new EmpleadoPorHoras("Ana", "Ponce", "22222222", 300.00, 40.0e);
		EmpleadoPorComision empleadoPorComision = new EmpleadoPorComision("Susana", "Montoya", "33333333", 500000.0, 0.10);
		EmpleadoBaseMasComision empleadoBaseMasComision = new EmpleadoBaseMasComision("Pablo", "Fuente", "44444444",300000.0, 0.04, 30000.0);

		System.out.println("Empleados procesados por separado:\n");


		System.out.printf("%s %n%n %s %n%n %s %n%n %s %n",empleadoAsalariado,empleadoPorHoras,empleadoPorComision,empleadoBaseMasComision);


		ArrayList<Empleado> empleados = new ArrayList<Empleado>();

		empleados.add(empleadoAsalariado);
		empleados.add(empleadoPorHoras);
		empleados.add(empleadoPorComision);
		empleados.add(empleadoBaseMasComision);

		System.out.println("\n\nEmpleados procesados en forma polimorfica:");

		for (Empleado empleadoActual : empleados) {
			System.out.println("\n"+empleadoActual); // invoca a toString

			if (empleadoActual instanceof EmpleadoBaseMasComision) {
				EmpleadoBaseMasComision empleado = (EmpleadoBaseMasComision) empleadoActual;

				empleado.setSalarioBase(1.10 * empleado.getSalarioBase());
				System.out.printf("el nuevo salario base con 10%% de aumento es: $%,.2f%n", empleado.getSalarioBase());
			} 
		} 


		for (int j = 0; j < empleados.size(); j++) {
			System.out.printf("El empleado %d es un %s%n", j, empleados.get(j).getClass().getName());
		}
	}

}
