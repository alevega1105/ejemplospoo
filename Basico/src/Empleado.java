public abstract class Empleado {
	private final String nombre;
	private final String apellido;
	private final String dni;

	public Empleado(String nombre, String apellido, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}
	@Override
	public String toString() {
		return getNombre()+" "+getApellido()+ "\n DNI: "+getDni()+ "\n ingresos: $"+ ingresos();
	}

	public abstract Double ingresos(); 
	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}


} 