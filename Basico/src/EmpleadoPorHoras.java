public class EmpleadoPorHoras extends Empleado {
	private double sueldo; 
	private double horas; 

	public EmpleadoPorHoras(String nombre, String apellido, String dni, double sueldo,
			double horas) {
		super(nombre, apellido, dni);
		this.sueldo = sueldo;
		this.horas = horas;
	}

	
	@Override
	public Double ingresos() {
		if (getHoras() <= 40) // no hay tiempo extra
			return getSueldo() * getHoras();
		else
			return 40 * getSueldo() + (getHoras() - 40) * getSueldo() * 1.5;
	}

	@Override
	public String toString() {
		return "empleado por horas: "+ super.toString()+ "\n sueldo: $" +getSueldo()+ "\n horas: "+getHoras();
	}
	
	public void setSueldo(double sueldo) {

		this.sueldo = sueldo;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setHoras(double horas) {
			this.horas = horas;
	}

	public double getHoras() {
		return horas;
	}


} 