
public abstract class Empleado {
	private String nombre, apellido, dni;

	public Empleado(String nombre, String apellido, String dni) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}
	
	public abstract Double ingresos();
	

	@Override
	public String toString() {
		return getNombre()+" "+ getApellido()+ " \n DNI: "+getDni()+ "\n ingresos: $"+ ingresos();
	}


	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}
	
}
