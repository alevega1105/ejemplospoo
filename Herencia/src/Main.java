import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		EmpleadoAsalariado empleadoAsalariado = new EmpleadoAsalariado("Juan", "Sosa", "11111111", 20000.0);
		EmpleadoPorHoras empleadoPorHoras = new EmpleadoPorHoras("Ana", "Ponce", "22222222", 300.0, 40.0);
		EmpleadoPorComision empleadoPorComision = new EmpleadoPorComision("Susana", "Montoya", "3333333333", 500000.0, 0.10);
		EmpleadoBaseMasComision empleadoBaseMasComision = new EmpleadoBaseMasComision("Pablo", "Fuente", "44444444", 300000.0, 0.04, 30000.0);
		
		System.out.println("Empleados procesados por separado: \n");
		
		System.out.printf("%s %n%n %s %n%n %s %n%n %s %n",empleadoAsalariado,empleadoPorHoras, empleadoPorComision, empleadoBaseMasComision);
		
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		empleados.add(empleadoAsalariado);
		empleados.add(empleadoPorHoras);
		empleados.add(empleadoPorComision);
		empleados.add(empleadoBaseMasComision);
		
		System.out.println("\n\n Empleados procesados en forma polimorfica: ");
		for (Empleado empleado : empleados) {
			System.out.println("\n"+ empleado);
			
			if (empleado instanceof EmpleadoBaseMasComision) {
				EmpleadoBaseMasComision empleadobmc = (EmpleadoBaseMasComision) empleado;
				
				empleadobmc.setSalarioBase(1.10*empleadobmc.getSalarioBase());
				System.out.printf("el nuevo salario base con 10%% de aumento es: $%,.2f%n", empleadobmc.getSalarioBase());
			}
		}
		
		for (int i = 0; i < empleados.size(); i++) {
			System.out.printf("El empleado %d es un %s%n",i, empleados.get(i).getClass().getName());
		}
	}

}
