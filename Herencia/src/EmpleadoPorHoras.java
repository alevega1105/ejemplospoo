
public class EmpleadoPorHoras extends Empleado {

	private Double sueldo, horas;

	public EmpleadoPorHoras(String nombre, String apellido, String dni, Double sueldo, Double horas) {
		super(nombre, apellido, dni);
		this.sueldo = sueldo;
		this.horas = horas;

	}

	public Double ingresos() {
		if (getHoras() <= 40) {
			return getSueldo() * getHoras();
		} else {
			return 40 * getSueldo() + (getHoras() - 40) * getSueldo() * 1.5;
		}
	}

	public String toString() {
		return "empleado por horas: " + super.toString() + "\n sueldo: $" + getSueldo() + " \n horas: " + getHoras();
	}

	public Double getSueldo() {
		return sueldo;
	}

	public Double getHoras() {
		return horas;
	}

}
