
public class EmpleadoPorComision extends Empleado {
	private Double ventasBrutas, tarifaComision;

	public EmpleadoPorComision(String nombre, String apellido, String dni, Double ventasBrutas, Double tarifaComision) {
		super(nombre, apellido, dni);
		this.ventasBrutas = ventasBrutas;
		this.tarifaComision = tarifaComision;
	}
	
	public Double ingresos() {
		return getVentasBrutas()*getTarifaComision();
	}
	

	@Override
	public String toString() {
		return "empleado por comisi�n: "+ super.toString()+ "\n ventas brutas: $"+ getVentasBrutas()+"\n comisi�n: "+getTarifaComision();
	}

	public Double getVentasBrutas() {
		return ventasBrutas;
	}

	public void setVentasBrutas(Double ventasBrutas) {
		this.ventasBrutas = ventasBrutas;
	}

	public Double getTarifaComision() {
		return tarifaComision;
	}

	public void setTarifaComision(Double tarifaComision) {
		this.tarifaComision = tarifaComision;
	}



}
