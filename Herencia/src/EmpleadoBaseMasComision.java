
public class EmpleadoBaseMasComision extends EmpleadoPorComision {

	private Double salarioBase;

	public EmpleadoBaseMasComision(String nombre, String apellido, String dni, Double ventasBrutas,
			Double tarifaComision, Double salarioBase) {
		super(nombre, apellido, dni, ventasBrutas, tarifaComision);
		this.salarioBase = salarioBase;
	}
	
	public Double ingresos() {
		return getSalarioBase()+ super.ingresos();
	}
	
	@Override
	public String toString() {
		return "empleado base + comisi�n: "+super.toString()+ "  \n salario base: $"+ getSalarioBase();
	}
	
	

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public Double getSalarioBase() {
		return salarioBase;
	}
	

}
