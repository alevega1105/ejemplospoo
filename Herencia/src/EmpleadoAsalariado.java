
public class EmpleadoAsalariado extends Empleado {

	private Double salarioSemanal;
	
	

	public EmpleadoAsalariado(String nombre, String apellido, String dni, Double salarioSemanal) {
		super(nombre, apellido, dni);
		this.salarioSemanal = salarioSemanal;
	}
	
	@Override
	public Double ingresos() {
		
		return getSalarioSemanal();
	}
	
	public String toString() {
		return "empleado asalariado: "+ super.toString();
	}
	

	public Double getSalarioSemanal() {
		return salarioSemanal;
	}

	public void setSalarioSemanal(Double salarioSemanal) {
		this.salarioSemanal = salarioSemanal;
	}


	
	
}
