package Modelo;
import java.io.File;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

public class Reproductor {

	private BasicPlayer repro;

	public Reproductor() {
		super();
		repro = new BasicPlayer();
	}
	
	public void abrir(String ruta) throws BasicPlayerException {
		repro.open(new File(ruta));
	}
	
	public void play() throws BasicPlayerException {
		repro.play();
	}
	
	public void stop() throws BasicPlayerException {
		repro.stop();
	}
	
	public void pausa() throws BasicPlayerException {
		repro.pause();
	}
	
	public void resume() throws BasicPlayerException {
		repro.resume();
	}

	public BasicPlayer getRepro() {
		return repro;
	}

	public void setRepro(BasicPlayer repro) {
		this.repro = repro;
	}
	
	
}
