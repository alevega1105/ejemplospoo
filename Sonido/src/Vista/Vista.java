package Vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.Controlador;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JProgressBar;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private Controlador controlador;
	private JButton btnAbrir;
	private JButton btnPlay;
	private JButton btnStop;
	private JButton btnPause;
	private JButton btnContinuar;
	private JProgressBar progressBar;

	public Vista(Controlador controlador) {
		this.setControlador(controlador);
		setTitle("Reproductor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(10, 11, 317, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnAbrir = new JButton("Abrir");
		btnAbrir.setBounds(337, 10, 89, 23);
		btnAbrir.addActionListener(this.getControlador());
		contentPane.add(btnAbrir);
		
		btnPlay = new JButton("Play");
		btnPlay.setBounds(40, 66, 89, 23);
		btnPlay.addActionListener(this.getControlador());
		contentPane.add(btnPlay);
		
		btnStop = new JButton("Stop");
		btnStop.setBounds(139, 66, 89, 23);
		btnStop.addActionListener(this.getControlador());
		contentPane.add(btnStop);
		
		btnPause = new JButton("Pause");
		btnPause.setBounds(238, 66, 89, 23);
		btnPause.addActionListener(this.getControlador());
		contentPane.add(btnPause);
		
		btnContinuar = new JButton("Continuar");
		btnContinuar.setBounds(337, 66, 89, 23);
		btnContinuar.addActionListener(this.getControlador());
		contentPane.add(btnContinuar);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(10, 42, 317, 13);
		contentPane.add(progressBar);
		
	}

	public Controlador getControlador() {
		return controlador;
	}

	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JButton getBtnAbrir() {
		return btnAbrir;
	}

	public void setBtnAbrir(JButton btnAbrir) {
		this.btnAbrir = btnAbrir;
	}

	public JButton getBtnPlay() {
		return btnPlay;
	}

	public void setBtnPlay(JButton btnPlay) {
		this.btnPlay = btnPlay;
	}

	public JButton getBtnStop() {
		return btnStop;
	}

	public void setBtnStop(JButton btnStop) {
		this.btnStop = btnStop;
	}

	public JButton getBtnPause() {
		return btnPause;
	}

	public void setBtnPause(JButton btnPause) {
		this.btnPause = btnPause;
	}

	public JButton getBtnContinuar() {
		return btnContinuar;
	}

	public void setBtnContinuar(JButton btnContinuar) {
		this.btnContinuar = btnContinuar;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}
	
}
