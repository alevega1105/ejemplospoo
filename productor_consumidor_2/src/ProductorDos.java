import java.security.SecureRandom;

public class ProductorDos extends Thread {
	private RecursoCompartido rc;

	
	public ProductorDos(RecursoCompartido rc) {
		super();
		this.rc = rc;
	}
	
	
	public void run() {
		SecureRandom r = new SecureRandom();
		for (int i = 0; i < r.nextInt(20); i++) {
			this.getRc().produccionNro(r.nextInt(100));
		}
		this.getRc().produccionNro(null);
	}
	
	public RecursoCompartido getRc() {
		return rc;
	}

	public void setRc(RecursoCompartido rc) {
		this.rc = rc;
	}

	
	

}
