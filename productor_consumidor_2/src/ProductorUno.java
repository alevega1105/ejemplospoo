import java.security.SecureRandom;

public class ProductorUno extends Thread {
	private RecursoCompartido rc;

	
	public ProductorUno(RecursoCompartido rc) {
		super();
		this.rc = rc;
	}
	
	
	public void run() {
		SecureRandom r = new SecureRandom();
		for (int i = 0; i < r.nextInt(10); i++) {
			this.getRc().produccionNro(r.nextInt(10));
		}
		
		this.getRc().produccionNro(null);
		
	}
	
	public RecursoCompartido getRc() {
		return rc;
	}

	public void setRc(RecursoCompartido rc) {
		this.rc = rc;
	}

	
	

}
