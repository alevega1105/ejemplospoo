
public class ApliProductorConsumidor {

	public static void main(String[] args) {
		RecursoCompartido rc = new RecursoCompartido();
		
		ProductorUno p1 = new ProductorUno(rc);
		p1.setName("P1");
		ProductorUno p2 = new ProductorUno(rc);
		p2.setName("P1-2");
		
		p1.start();
		p2.start();
	

		Consumidor c1 = new Consumidor(rc);
		c1.setName("C1");
	
		
		c1.start();
	
		
	}

}
