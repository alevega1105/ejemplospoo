import java.security.SecureRandom;
import java.util.Random;

public class EjemplosBasicos {

	public static void main(String[] args) {
		Math.pow(5, 2);
		Math.sqrt(900);
		Math.abs(23.32);
		Math.ceil(9.3);
		Math.floor(9.4);
		Math.cos(0.0);
		Math.sin(0.0);
		Math.tan(0.0);
		Math.log(Math.E);
		Math.max(2.3, 12.7);
		Math.min(2.3, 12.5);
		
		
		Random ran = new Random();
		ran.nextInt();
		
		
		SecureRandom sran = new SecureRandom();
		System.out.println(sran.nextInt(2));
		
		String cadena = "esto es un String";
		String cadena2 = new String("hola");
		
	/*	for (int i = 0; i < cadena.length(); i++) {
			System.out.println(cadena.charAt(i));
		}*/
		
		/*for (char str : cadena.toCharArray()) {
			System.out.println(str);
		}*/
		
		System.out.println(cadena.substring(0,4));
		
		if (cadena2.equalsIgnoreCase( "HOLA") ){
			System.out.println("con equals son iguales");
		}else {
			System.out.println("con equals NO son iguales");
		}
	

	}

}
