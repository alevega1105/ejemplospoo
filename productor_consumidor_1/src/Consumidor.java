import java.util.ArrayList;

public class Consumidor extends Thread {
	private RecursoCompartido rc;
	
	private ArrayList<Integer> numeros;

	public Consumidor(RecursoCompartido rc) {
		super();
		this.rc = rc;
		this.numeros = new ArrayList<Integer>();
	}
	
	public void run() {
		
		Integer numero; 
		Integer cont = 0;
		while (cont<2) {
			numero= this.getRc().consumirNro();
			if(numero ==null) {
				cont++;
				System.out.println(cont);
			}else {
				getNumeros().add(numero);
			}
		}
		mostrarInforme();
		
	}
	

	private void mostrarInforme() {
		Integer sumatoria = 0;
		for (Integer numero : numeros) {
			sumatoria = sumatoria + numero;
		}
		System.out.println("el promedio de los numeros es: "+ sumatoria/getNumeros().size());
		
	}

	public RecursoCompartido getRc() {
		return rc;
	}

	public void setRc(RecursoCompartido rc) {
		this.rc = rc;
	}

	public ArrayList<Integer> getNumeros() {
		return numeros;
	}

	public void setNumeros(ArrayList<Integer> numeros) {
		this.numeros = numeros;
	}

}
