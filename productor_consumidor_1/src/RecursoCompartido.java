import java.util.ArrayList;

public class RecursoCompartido {
	private ArrayList<Integer> rc; //limitado a 2
	
	

	public RecursoCompartido() {
		super();
		this.rc = new ArrayList<Integer>();
	}
	
	public synchronized void produccionNro(Integer nro) {
		//controlar el espacio de 2
		while(this.getRc().size()>=2) {
			try {
				System.out.println("El productor "+ Thread.currentThread().getName() + " esta esperando");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		this.getRc().add(nro);
		notifyAll();
		
		if(nro !=null ) {
		System.out.println("El productor "+ Thread.currentThread().getName()+ " ingreso el nro: "+ nro);}
	}
	
	public synchronized Integer consumirNro() {
		//controlar que no este vacio.
		while(this.getRc().size()==0) {
			try {
				System.out.println("El consumidor "+ Thread.currentThread().getName() + " esta esperando");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		Integer nro = this.getRc().remove(0);
		
		if(nro !=null) {
		
		System.out.println("El consumidor "+ Thread.currentThread().getName()+ " obtuvo el nro: "+ nro);
		}
		notifyAll();
		return nro;
	}
	
	
	

	public ArrayList<Integer> getRc() {
		return rc;
	}

	public void setRc(ArrayList<Integer> rc) {
		this.rc = rc;
	}

}
