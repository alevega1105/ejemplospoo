package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import vista.Vista;

public class Controlador implements ActionListener, KeyListener {
	private Vista vista;

	public Controlador() {
		vista = new Vista(this);
		vista.setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		mostrarInfo(e, "KEY TYPED: ");

	}

	@Override
	public void keyPressed(KeyEvent e) {
		mostrarInfo(e, "KEY PRESSED: ");

	}

	@Override
	public void keyReleased(KeyEvent e) {
		mostrarInfo(e, "KEY RELEASED: ");

	}

	private void mostrarInfo(KeyEvent e, String evento) {
		int id = e.getID();
		String keyString;
		if (id == KeyEvent.KEY_TYPED) {
			char c = e.getKeyChar();
			keyString = "caracter = '" + c + "'";
		} else {
			int keyCode = e.getKeyCode();
			keyString = "c�digo = " + keyCode + " (" + KeyEvent.getKeyText(keyCode) + ")";
		}

		String modString = "modificador = " + e.getModifiersEx();
		String tmpString = KeyEvent.getModifiersExText(e.getModifiersEx());
		if (tmpString.length() > 0) {
			modString += " (" + tmpString + ")";
		} else {
			modString += " (sin modificador)";
		}

		String actionString = "acci�n? ";
		if (e.isActionKey()) {
			actionString += "SI";
		} else {
			actionString += "NO";
		}
		String locationString = "Ubicaci�n: ";
		switch (e.getKeyLocation()) {
		case KeyEvent.KEY_LOCATION_STANDARD: {
			locationString += "standard";
			break;
		}
		case KeyEvent.KEY_LOCATION_LEFT: {
			locationString += "izquierda";
			break;
		}
		case KeyEvent. KEY_LOCATION_RIGHT: {
			locationString += "derecha";
			break;
		}
		case KeyEvent.KEY_LOCATION_NUMPAD: {
			locationString += "numpad";
			break;
		}
		case KeyEvent.KEY_LOCATION_UNKNOWN: {
			locationString += "desconocido";
			break;
		 }
		}
		
		getVista().getTextArea().append( evento + "\n\t"
						+keyString + "\n\t"
						+modString + "\n\t"
						+actionString + "\n\t"
						+locationString + "\n");
		getVista().getTextArea().setCaretPosition(getVista().getTextArea().getDocument().getLength());

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		getVista().getTextArea().setText("");
		getVista().getTxtInput().setText("");

		getVista().getTxtInput().requestFocusInWindow();

	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

}
