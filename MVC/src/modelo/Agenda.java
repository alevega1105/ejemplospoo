package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Agenda {
	
	ArrayList<Persona> agenda = new ArrayList<Persona>();

	public Agenda() {
		super();
	}

	public Boolean agregar(Persona p) {
		return getAgenda().add(p);
	}
	
	public void modificar(Persona persona) {
		for (int i = 0; i < this.agenda.size(); i++) {
			if(agenda.get(i).getId() == persona.getId()) {
				System.out.println("hola");
				agenda.get(i).setApellido(persona.getApellido());
				agenda.get(i).setNombre(persona.getNombre());
				agenda.get(i).setTelefono(persona.getTelefono());
			}
		}
	}
	
	public void eliminar(Integer id) {
		for (int i = 0; i < this.agenda.size(); i++) {
			if(agenda.get(i).getId() == id) {
				this.agenda.remove(i);
			}
		}
	}
	
	public Persona obtener(Integer id) {
		for (int i = 0; i < this.agenda.size(); i++) {
			if(agenda.get(i).getId() == id) {
				return agenda.get(i);
			}
		}
		//Mostrar msj de que no existe
		return null;
	}
	
	public String toString() {
		String cadena= "Nombre\tApellido\tTel�fono\n";
		for (Persona persona : getAgenda()) {
			cadena= cadena+ persona.getNombre()+"\t"+ persona.getApellido()+"\t\t"+persona.getTelefono()+"\n"; 
		}
		return cadena; 
	}
	
	public ArrayList<Persona> getAgenda() {
		return agenda;
	}

	public void setAgenda(ArrayList<Persona> agenda) {
		this.agenda = agenda;
	}
	
	//Metodo hecho por no tener base de datos
	public Integer maxId() {
		return this.agenda.size()+1;
	}
	

}
