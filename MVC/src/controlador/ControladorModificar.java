package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Agenda;
import modelo.Persona;
import vista.VistaModificarUsuario;

public class ControladorModificar implements ActionListener{
	private Agenda agenda;
	private Integer id;
	private VistaModificarUsuario vistaModificar;
	private Persona persona;
	
	public ControladorModificar(Agenda agenda, Integer id) {
		super();
		this.agenda = agenda;
		persona = this.agenda.obtener(id);
		vistaModificar = new VistaModificarUsuario(this,"Modificar Usuario", persona);
		vistaModificar.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(vistaModificar.getBtnAceptar())) {
			
			persona.setNombre(this.vistaModificar.getTxtNombre().getText());
			persona.setApellido(this.vistaModificar.getTxtApellido().getText());
			persona.setTelefono(this.vistaModificar.getTxtTelefono().getText());
			agenda.modificar(persona);	
			
			new ControladorUsuario(agenda);
			this.getVistaModificar().dispose();
		}else if(e.getSource().equals(this.getVistaModificar().getBtnCancelar())) {
			new ControladorUsuario(agenda);
			this.getVistaModificar().dispose();
		}
		
	}

	public VistaModificarUsuario getVistaModificar() {
		return vistaModificar;
	}

	public void setVistaModificar(VistaModificarUsuario vistaModificar) {
		this.vistaModificar = vistaModificar;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}


}
