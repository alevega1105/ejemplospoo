package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.Agenda;
import modelo.Persona;
import vista.VistaInformacionUsuario;
import vista.VistaModificarUsuario;
import vista.VistaNuevoUsuario;
import vista.VistaUsuarios;

public class ControladorUsuario implements ActionListener, KeyListener, ListSelectionListener{
	private Agenda agenda;
	private VistaUsuarios vistaUsuarios;
	
	public ControladorUsuario(Agenda agenda) {
		super();
		this.agenda = agenda;
		this.vistaUsuarios = new VistaUsuarios(this);
		for (Persona persona : this.getAgenda().getAgenda()) {
			Object [] row = {String.valueOf(persona.getId()),persona.getNombre(),persona.getApellido(),persona.getTelefono()};
			this.getVistaUsuarios().getModel().addRow(row);
		}
		this.vistaUsuarios.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(getVistaUsuarios().getBtnNuevo())) {
			new ControladorNuevo(this.agenda);
			this.getVistaUsuarios().dispose();
		}else if(e.getSource().equals(getVistaUsuarios().getBtnModificar())) {
			new ControladorModificar(this.agenda, Integer.valueOf(this.getVistaUsuarios().getModel().getValueAt(this.getVistaUsuarios().getTable().getSelectedRow(), 0).toString()));
			this.getVistaUsuarios().dispose();
		}else if(e.getSource().equals(getVistaUsuarios().getBtnEliminar())) {
			int resp = JOptionPane.showConfirmDialog(null, "�Estas seguro de eliminar el usuario?", "Eliminar usuario", JOptionPane.YES_NO_OPTION);
			if(resp == 0) {
				this.agenda.eliminar(Integer.valueOf(this.getVistaUsuarios().getModel().getValueAt(this.getVistaUsuarios().getTable().getSelectedRow(), 0).toString()));
				this.getVistaUsuarios().getModel().removeRow(this.getVistaUsuarios().getTable().getSelectedRow());
				JOptionPane.showMessageDialog(null, "Se elimino correctamente el usuario");
			}
		}else if(e.getSource().equals(getVistaUsuarios().getBtnInformacin())) {
			new ControladorInformacion(this);	
		}
		
	}
	
	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	
	public VistaUsuarios getVistaUsuarios() {
		return vistaUsuarios;
	}

	public void setVistaUsuarios(VistaUsuarios vistaUsuarios) {
		this.vistaUsuarios = vistaUsuarios;
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		this.search();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		this.search();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		this.getVistaUsuarios().getBtnModificar().setEnabled(true);
		this.getVistaUsuarios().getBtnInformacin().setEnabled(true);
		this.getVistaUsuarios().getBtnEliminar().setEnabled(true);
	}
	

	public void search() {
		TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(this.getVistaUsuarios().getModel());
		this.getVistaUsuarios().getTable().setRowSorter(tr);
		
		tr.setRowFilter(RowFilter.regexFilter(this.getVistaUsuarios().getTxtBuscar().getText()));
	}


}
