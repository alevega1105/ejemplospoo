package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Agenda;
import modelo.Persona;
import vista.VistaNuevoUsuario;

public class ControladorNuevo implements ActionListener{
	private Agenda agenda;
	private VistaNuevoUsuario vistaNuevo;
	
	public ControladorNuevo(Agenda agenda) {
		super();
		this.agenda = agenda;
		vistaNuevo =  new VistaNuevoUsuario(this,"Nuevo Usuario");
		vistaNuevo.setControlador(this);
		vistaNuevo.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(vistaNuevo.getBtnAceptar())) {
			Integer id = agenda.maxId();
			agenda.agregar(new Persona(id, this.vistaNuevo.getTxtNombre().getText(), this.vistaNuevo.getTxtApellido().getText(),this.vistaNuevo.getTxtTelefono().getText()));
			new ControladorUsuario(agenda);
			this.vistaNuevo.dispose();
		}else if(e.getSource().equals(this.getVistaNuevo().getBtnCancelar())) {
			new ControladorUsuario(agenda);
			this.getVistaNuevo().dispose();
		}
		
	}

	public VistaNuevoUsuario getVistaNuevo() {
		return vistaNuevo;
	}

	public void setVistaNuevo(VistaNuevoUsuario vistaNuevo) {
		this.vistaNuevo = vistaNuevo;
	}

	
}
