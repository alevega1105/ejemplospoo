package controlador;

import java.awt.EventQueue;

import modelo.Agenda;
import modelo.Persona;

public class Main {

	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorUsuario(new Agenda());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
