package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vista.VistaInformacionUsuario;

public class ControladorInformacion  implements ActionListener{
	
	private VistaInformacionUsuario vistaInfo;
	private ControladorUsuario cu;

	public ControladorInformacion(ControladorUsuario cu) {
		super();
		this.cu = cu;
		vistaInfo = new VistaInformacionUsuario(this, "Información Usuario");
		vistaInfo.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(vistaInfo.getBtnAceptar())) {
			this.getCu().getVistaUsuarios().setVisible(true);
			this.getVistaInfo().dispose();
		}
		
	}

	public VistaInformacionUsuario getVistaInfo() {
		return vistaInfo;
	}

	public void setVistaInfo(VistaInformacionUsuario vistaInfo) {
		this.vistaInfo = vistaInfo;
	}

	public ControladorUsuario getCu() {
		return cu;
	}

	public void setCu(ControladorUsuario cu) {
		this.cu = cu;
	}
	
	

	
}
