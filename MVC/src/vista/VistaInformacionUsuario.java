package vista;

import controlador.ControladorInformacion;

public class VistaInformacionUsuario extends Vista{
	
	ControladorInformacion controlador;

	public VistaInformacionUsuario(ControladorInformacion c, String titulo) {
		super();
		super.getLblTitulo().setText(titulo);
		this.setControlador(c);
		
		super.getBtnAceptar().addActionListener(c);
		
		super.getLblCamposObligatorios().setVisible(false);
		super.getTxtNombre().setEnabled(false);
		super.getTxtApellido().setEnabled(false);
		super.getTxtTelefono().setEnabled(false);
		super.getBtnCancelar().setVisible(false);
		
		int selectrow = this.getControlador().getCu().getVistaUsuarios().getTable().getSelectedRow();
		super.getTxtNombre().setText(this.getControlador().getCu().getVistaUsuarios().getTable().getValueAt(selectrow, 1).toString());
		super.getTxtApellido().setText(this.getControlador().getCu().getVistaUsuarios().getTable().getValueAt(selectrow, 2).toString());
		super.getTxtTelefono().setText(this.getControlador().getCu().getVistaUsuarios().getTable().getValueAt(selectrow, 3).toString());
	
	}

	public ControladorInformacion getControlador() {
		return controlador;
	}

	public void setControlador(ControladorInformacion controlador) {
		this.controlador = controlador;
	}
	
	

}
