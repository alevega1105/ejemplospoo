package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JButton;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controlador.ControladorNuevo;
import controlador.ControladorUsuario;

import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.JSplitPane;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.DropMode;
import javax.swing.SpringLayout;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollBar;
import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaNuevoUsuario extends Vista {
	
	ControladorNuevo controlador;

	public VistaNuevoUsuario(ControladorNuevo c, String titulo) {
		super();
		super.getLblTitulo().setText(titulo);
		this.setControlador(c);
		super.getBtnAceptar().addActionListener(c);
		super.getBtnCancelar().addActionListener(c);
	}

	public ControladorNuevo getControlador() {
		return controlador;
	}

	public void setControlador(ControladorNuevo controlador) {
		this.controlador = controlador;
	}
	
	
	
}
