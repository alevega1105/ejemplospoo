package vista;

import controlador.ControladorModificar;
import modelo.Persona;

public class VistaModificarUsuario extends Vista{
	
	ControladorModificar controlador;

	public VistaModificarUsuario(ControladorModificar c, String titulo, Persona persona) {
		super();
		super.getLblTitulo().setText(titulo);
		this.setControlador(c);
		super.getBtnAceptar().addActionListener(c);
		super.getBtnCancelar().addActionListener(c);
		super.getTxtNombre().setText(persona.getNombre());
		super.getTxtApellido().setText(persona.getApellido());
		super.getTxtTelefono().setText(persona.getTelefono());
	}

	public ControladorModificar getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModificar controlador) {
		this.controlador = controlador;
	}

	
	
	
}
