package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Default;

import controlador.ControladorUsuario;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;

public class VistaUsuarios extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private ControladorUsuario controlador;
	private String data[][] = {};
	
	private JButton btnNuevo;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnInformacin;
	private DefaultTableModel model;
	private JTextField txtBuscar;

	
	public VistaUsuarios(ControladorUsuario controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuarios = new JLabel("Usuarios");
		lblUsuarios.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblUsuarios.setBounds(111, 8, 121, 19);
		contentPane.add(lblUsuarios);
		
		JLabel lblBuscar = new JLabel("Buscar: ");
		lblBuscar.setBounds(50, 11, 100, 55);
		contentPane.add(lblBuscar);
		
		txtBuscar = new JTextField();
		txtBuscar.setBounds(100,30,200,20);
		txtBuscar.addKeyListener(controlador);
		contentPane.add(txtBuscar);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.setBounds(335, 45, 89, 23);
		btnNuevo.addActionListener(controlador);
		contentPane.add(btnNuevo);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(335, 77, 89, 23);
		btnModificar.addActionListener(controlador);
		btnModificar.setEnabled(false);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(335, 111, 89, 23);
		btnEliminar.addActionListener(controlador);
		btnEliminar.setEnabled(false);
		contentPane.add(btnEliminar);
		
		btnInformacin = new JButton("Informaci\u00F3n");
		btnInformacin.setBounds(335, 145, 89, 23);
		btnInformacin.addActionListener(controlador);
		btnInformacin.setEnabled(false);
		contentPane.add(btnInformacin);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 41, 315, 209);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 295, 187);

	    String header[] = {"ID","Nombre", "Apellido", "Telefono"};
	    model = new DefaultTableModel(this.getData(),header);
	    
	    table = new JTable(model);
	    table.setDefaultEditor(Object.class, null);
	    table.getColumnModel().getColumn(0).setMaxWidth(0);
	    table.getColumnModel().getColumn(0).setMinWidth(0);
	    table.getColumnModel().getColumn(0).setPreferredWidth(0);
	    
	    table.setBounds(10, 41, 40, 50);
		table.setVisible(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(getControlador());
		
		scrollPane.setViewportView(table);
		
		panel.add(scrollPane);
		
	}

	public String[][] getData() {
		return data;
	}


	public void setData(String[][] data) {
		this.data = data;
	}


	public ControladorUsuario getControlador() {
		return controlador;
	}


	public void setControlador(ControladorUsuario controlador) {
		this.controlador = controlador;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnNuevo() {
		return btnNuevo;
	}

	public void setBtnNuevo(JButton btnNuevo) {
		this.btnNuevo = btnNuevo;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnInformacin() {
		return btnInformacin;
	}

	public void setBtnInformacin(JButton btnInformacin) {
		this.btnInformacin = btnInformacin;
	}

	public JTextField getTxtBuscar() {
		return txtBuscar;
	}

	public void setTxtBuscar(JTextField txtBuscar) {
		this.txtBuscar = txtBuscar;
	}
	
		
}
