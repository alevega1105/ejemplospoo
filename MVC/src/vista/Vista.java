package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controlador.ControladorUsuario;

public class Vista extends JFrame{

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtTelefono;
	private JButton btnAceptar;
	private JLabel errorApellido;
	private JLabel errorTelefono;
	private JLabel errorNombre;
	private JLabel lblTitulo;
	private JButton btnCancelar;
	private JLabel lblCamposObligatorios;


	public Vista() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 370, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		lblTitulo = new JLabel("     Nuevo usuario");
		lblTitulo.setForeground(Color.BLACK);
		lblTitulo.setHorizontalAlignment(SwingConstants.LEFT);
		lblTitulo.setFont(new Font("Segoe UI", Font.BOLD, 17));
		contentPane.add(lblTitulo, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBackground(SystemColor.control);
		panel.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(SystemColor.control);
		panel.add(btnCancelar);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		contentPane.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		contentPane.add(horizontalStrut_1, BorderLayout.EAST);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("*Nombre:");
		lblNewLabel.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		
		lblNewLabel.setBounds(10, 20, 115, 14);
		panel_1.add(lblNewLabel);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(10, 45, 243, 28);
		panel_1.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("*Apellido:");
		lblApellido.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		lblApellido.setBounds(10, 92, 126, 14);
		
		panel_1.add(lblApellido);
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(10, 117, 243, 28);
		panel_1.add(txtApellido);
		
		JLabel lblTelfono = new JLabel("*Tel\u00E9fono:");
		lblTelfono.setForeground(Color.BLACK);
		lblTelfono.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		lblTelfono.setBounds(10, 170, 115, 14);
		panel_1.add(lblTelfono);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(10, 195, 243, 28);
		panel_1.add(txtTelefono);
		
		errorNombre = new JLabel("Ingrese un nombre por favor.");
		errorNombre.setForeground(Color.RED);
		errorNombre.setBounds(68, 78, 185, 14);
		panel_1.add(errorNombre);
		
		errorApellido = new JLabel("Ingrese un apellido por favor.");
		errorApellido.setForeground(Color.RED);
		errorApellido.setBounds(68, 156, 185, 14);
		panel_1.add(errorApellido);
		
		errorTelefono = new JLabel("Ingrese un telefono por favor.");
		errorTelefono.setForeground(Color.RED);
		errorTelefono.setBounds(68, 234, 185, 14);
		panel_1.add(errorTelefono);
		
		lblCamposObligatorios = new JLabel("* Campos obligatorios");
		lblCamposObligatorios.setBounds(10, 281, 243, 14);
		panel_1.add(lblCamposObligatorios);
		
		this.setLocationRelativeTo(null);
		
		this.getErrorNombre().setVisible(false);
		this.getErrorApellido().setVisible(false);
		this.getErrorTelefono().setVisible(false);
	}

	public JLabel getLblTitulo() {
		return lblTitulo;
	}

	public void setLblTitulo(JLabel lblTitulo) {
		this.lblTitulo = lblTitulo;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JLabel getErrorApellido() {
		return errorApellido;
	}

	public void setErrorApellido(JLabel errorApellido) {
		this.errorApellido = errorApellido;
	}

	public JLabel getErrorTelefono() {
		return errorTelefono;
	}

	public void setErrorTelefono(JLabel errorTelefono) {
		this.errorTelefono = errorTelefono;
	}

	public JLabel getErrorNombre() {
		return errorNombre;
	}

	public void setErrorNombre(JLabel errorNombre) {
		this.errorNombre = errorNombre;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JLabel getLblCamposObligatorios() {
		return lblCamposObligatorios;
	}

	public void setLblCamposObligatorios(JLabel lblCamposObligatorios) {
		this.lblCamposObligatorios = lblCamposObligatorios;
	}
	
}
