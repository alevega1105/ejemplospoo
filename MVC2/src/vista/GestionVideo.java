package vista;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorGestionVideo;

public class GestionVideo extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private ControladorGestionVideo controlador;
	private JButton btnNuevo;
	private JScrollPane scrollPane;
	private JButton btnPreview;

	public GestionVideo(ControladorGestionVideo c) {
		setControlador(c);
		addWindowListener(this.getControlador());
		setTitle("Gesti\u00F3n de videos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 512);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Buscar:");
		lblNewLabel.setBounds(21, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(88, 8, 273, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 48, 498, 380);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Titulo",
				"Categoria"
			}
		));
		scrollPane.setViewportView(table);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(getControlador());
		btnNuevo.setBounds(442, 7, 77, 23);
		contentPane.add(btnNuevo);
		
		btnPreview = new JButton("Preview");
		btnPreview.addActionListener(getControlador());
		btnPreview.setBounds(430, 439, 89, 23);
		contentPane.add(btnPreview);
		setLocationRelativeTo(null);
	}

	public ControladorGestionVideo getControlador() {
		return controlador;
	}

	public void setControlador(ControladorGestionVideo controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnNuevo() {
		return btnNuevo;
	}

	public void setBtnNuevo(JButton btnNuevo) {
		this.btnNuevo = btnNuevo;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnPreview() {
		return btnPreview;
	}

	public void setBtnPreview(JButton btnPreview) {
		this.btnPreview = btnPreview;
	}
}
