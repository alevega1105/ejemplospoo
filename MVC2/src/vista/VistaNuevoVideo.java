package vista;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controlador.ControladorNuevoVideo;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

public class VistaNuevoVideo extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JComboBox comboCategoria;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private ControladorNuevoVideo controlador;
	private JLabel lblImagen;
	private JPanel panel;

	/**
	 * Create the frame.
	 */
	public VistaNuevoVideo(ControladorNuevoVideo c) {
		setControlador(c);
		setTitle("Nuevo Video");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 363, 381);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblNewLabel.setBounds(23, 63, 46, 14);
		contentPane.add(lblNewLabel);

		txtNombre = new JTextField();
		txtNombre.setBounds(120, 57, 170, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Nuevo video");
		lblNewLabel_1.setFont(new Font("SansSerif", Font.BOLD, 16));
		lblNewLabel_1.setBounds(23, 11, 432, 33);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Categoria");
		lblNewLabel_2.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(23, 109, 66, 14);
		contentPane.add(lblNewLabel_2);

		comboCategoria = new JComboBox();
		comboCategoria.setModel(new DefaultComboBoxModel(new String[] { "Popular", "Tendencia", "Serie" }));
		comboCategoria.setBounds(120, 105, 170, 22);
		contentPane.add(comboCategoria);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControlador());
		btnAceptar.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnAceptar.setBounds(58, 302, 89, 23);
		contentPane.add(btnAceptar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(getControlador());
		btnCancelar.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnCancelar.setBounds(180, 302, 89, 23);
		contentPane.add(btnCancelar);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(
				new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Imagen",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(78, 144, 182, 118);
		contentPane.add(panel);
		panel.setLayout(null);

		lblImagen = new JLabel("click para buscar");
		lblImagen.addMouseListener(this.getControlador());
		lblImagen.setBounds(7, 14, 170, 96);
		panel.add(lblImagen);
		lblImagen.setForeground(Color.BLACK);
		lblImagen.setBackground(Color.WHITE);
		lblImagen.setHorizontalAlignment(SwingConstants.CENTER);

		setLocationRelativeTo(null);
	}


	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JComboBox getComboCategoria() {
		return comboCategoria;
	}

	public void setComboCategoria(JComboBox comboCategoria) {
		this.comboCategoria = comboCategoria;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public ControladorNuevoVideo getControlador() {
		return controlador;
	}

	public void setControlador(ControladorNuevoVideo controlador) {
		this.controlador = controlador;
	}

	public JLabel getLblImagen() {
		return lblImagen;
	}

	public void setLblImagen(JLabel lblImagen) {
		this.lblImagen = lblImagen;
	}



}
