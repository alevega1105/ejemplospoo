package vista;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaPrincipal;
import modelo.Video;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private ArrayList<JLabel> videos = new ArrayList<JLabel>();
	private ControladorVistaPrincipal controlador;

	public VistaPrincipal(ControladorVistaPrincipal c) {
		setControlador(c);
		setForeground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1920, 1080);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(9, 9, 9));
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(".\\assets\\img\\vistaprincipal\\log.PNG"));
		lblNewLabel.setBounds(30, 11, 131, 88);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Inicio");
		lblNewLabel_1.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(171, 50, 46, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Series");
		lblNewLabel_2.setFont(new Font("SansSerif", Font.PLAIN, 17));
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setBounds(227, 50, 71, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_2_1 = new JLabel("Pel\u00EDcula");
		lblNewLabel_2_1.setForeground(Color.WHITE);
		lblNewLabel_2_1.setFont(new Font("SansSerif", Font.PLAIN, 17));
		lblNewLabel_2_1.setBounds(292, 50, 71, 14);
		contentPane.add(lblNewLabel_2_1);

		JLabel lblNewLabel_2_2 = new JLabel("M\u00E1s recientes");
		lblNewLabel_2_2.setForeground(Color.WHITE);
		lblNewLabel_2_2.setFont(new Font("SansSerif", Font.PLAIN, 17));
		lblNewLabel_2_2.setBounds(362, 50, 108, 14);
		contentPane.add(lblNewLabel_2_2);

		JLabel lblNewLabel_2_3 = new JLabel("Mi lista");
		lblNewLabel_2_3.setForeground(Color.WHITE);
		lblNewLabel_2_3.setFont(new Font("SansSerif", Font.PLAIN, 17));
		lblNewLabel_2_3.setBounds(478, 50, 71, 14);
		contentPane.add(lblNewLabel_2_3);

		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon(".\\assets\\img\\vistaprincipal\\menu.PNG"));
		lblNewLabel_3.setBounds(1670, 44, 134, 28);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon(".\\assets\\img\\vistaprincipal\\usuario.png"));
		lblNewLabel_4.setBounds(1817, 33, 55, 48);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Populares en Netflix");
		lblNewLabel_5.setFont(new Font("SansSerif", Font.BOLD, 25));
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setBounds(30, 110, 268, 42);
		contentPane.add(lblNewLabel_5);

		JLabel lblNewLabel_5_1 = new JLabel("Tendencias");
		lblNewLabel_5_1.setForeground(Color.WHITE);
		lblNewLabel_5_1.setFont(new Font("SansSerif", Font.BOLD, 25));
		lblNewLabel_5_1.setBounds(30, 360, 268, 42);
		contentPane.add(lblNewLabel_5_1);

		JLabel lblNewLabel_5_2 = new JLabel("Series");
		lblNewLabel_5_2.setForeground(Color.WHITE);
		lblNewLabel_5_2.setFont(new Font("SansSerif", Font.BOLD, 25));
		lblNewLabel_5_2.setBounds(30, 610, 268, 42);
		contentPane.add(lblNewLabel_5_2);

		actualizarVideos();

	}

	private void actualizarVideos() {
		ArrayList<Video> videos = getControlador().getModelo().consultar();
		Integer xPopular=30;
		Integer xTendencia=30;
		Integer xSerie=30;
		for (Video video : videos) {
			
			JLabel lbl = new JLabel("");
			lbl.setIcon(new ImageIcon(video.getImagen()));
			
			switch (video.getCategoria()) {
			case "Popular":
				lbl.setBounds(xPopular, 160, 341, 192);
				xPopular =xPopular+345;
				break;
			case "Tendencia":
				lbl.setBounds(xTendencia, 410, 341, 192);
				xTendencia =xTendencia+345;
				break;
			case "Serie":
				lbl.setBounds(xSerie, 660, 341, 192);
				xSerie =xSerie+345;
				break;
			}
			contentPane.add(lbl);
		}
	}

	



	public ArrayList<JLabel> getVideos() {
		return videos;
	}

	public void setVideos(ArrayList<JLabel> videos) {
		this.videos = videos;
	}

	public ControladorVistaPrincipal getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVistaPrincipal controlador) {
		this.controlador = controlador;
	}
}
