package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import modelo.ColeccionVideos;
import modelo.Video;
import vista.GestionVideo;

public class ControladorGestionVideo implements ActionListener, WindowListener {

	private ColeccionVideos coleccionVideos;
	private GestionVideo vista;

	public ControladorGestionVideo() {
		coleccionVideos = new ColeccionVideos();
		vista = new GestionVideo(this);
		vista.setVisible(true);
		actualizarModeloTabla();

	}

	private void actualizarModeloTabla() {
		DefaultTableModel modeloTabla = new DefaultTableModel();
		modeloTabla.addColumn("titulo");
		modeloTabla.addColumn("categoria");

		for (Video video : getColeccionVideos().consultar()) {
			Vector<String> vector = new Vector<String>();
			vector.add(video.getTitulo());
			vector.add(video.getCategoria());
			modeloTabla.addRow(vector);
		}

		getVista().getTable().setModel(modeloTabla);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnNuevo())) {
			new ControladorNuevoVideo(getColeccionVideos());
		}
		
		if(e.getSource().equals(getVista().getBtnPreview())) {
			new ControladorVistaPrincipal(getColeccionVideos());
		}
	}

	public ColeccionVideos getColeccionVideos() {
		return coleccionVideos;
	}

	public void setColeccionVideos(ColeccionVideos coleccionVideos) {
		this.coleccionVideos = coleccionVideos;
	}

	public GestionVideo getVista() {
		return vista;
	}

	public void setVista(GestionVideo vista) {
		this.vista = vista;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		actualizarModeloTabla();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

}
