package controlador;

import java.awt.EventQueue;

import vista.GestionVideo;

public class Main {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorGestionVideo();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
