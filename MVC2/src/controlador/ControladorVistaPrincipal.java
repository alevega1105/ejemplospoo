package controlador;

import modelo.ColeccionVideos;
import vista.VistaPrincipal;

public class ControladorVistaPrincipal {
	private ColeccionVideos modelo;
	private VistaPrincipal vista;
	
	
	
	public ControladorVistaPrincipal(ColeccionVideos modelo) {
	this.modelo = modelo;
	this.vista = new VistaPrincipal(this);
	this.vista.setVisible(true);
	}
	public ColeccionVideos getModelo() {
		return modelo;
	}
	public void setModelo(ColeccionVideos modelo) {
		this.modelo = modelo;
	}
	public VistaPrincipal getVista() {
		return vista;
	}
	public void setVista(VistaPrincipal vista) {
		this.vista = vista;
	}
	

}
