package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import modelo.ColeccionVideos;
import modelo.Video;
import vista.VistaNuevoVideo;

public class ControladorNuevoVideo implements ActionListener, MouseListener {
	private ColeccionVideos modelo;
	private VistaNuevoVideo vista;
	private File imagen;

	public ControladorNuevoVideo(ColeccionVideos coleccionVideos) {
		this.modelo = coleccionVideos;
		this.vista = new VistaNuevoVideo(this);
		this.vista.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(getVista().getBtnAceptar())) {
			Integer id = getModelo().getLastId() + 1;
			String titulo = getVista().getTxtNombre().getText();
			String pathImagen = ".\\assets\\img\\video"+id+".jpg";
			String categoria = getVista().getComboCategoria().getSelectedItem().toString();

			/* copiar imagen a una carpeta relativa */
			Path copia = Paths.get(pathImagen);
			Path pathOriginal = Paths.get(getImagen().getAbsolutePath());
			try {
				Files.copy(pathOriginal, copia, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			getModelo().agregar(new Video(id, titulo, pathImagen, categoria));
		}
		getVista().dispose();

	}

	@Override
	public void mousePressed(MouseEvent e) {
		JFileChooser f = new JFileChooser();
		f.setFileSelectionMode(JFileChooser.FILES_ONLY);
		f.showSaveDialog(getVista());

		if (f.getSelectedFile() != null) {
			ImageIcon imageIcon = new ImageIcon(new ImageIcon(f.getSelectedFile().getPath()).getImage()
					.getScaledInstance(170, 96, Image.SCALE_REPLICATE));

			getVista().getLblImagen().setIcon(imageIcon);
			setImagen(f.getSelectedFile());

		}
	}

	public ColeccionVideos getModelo() {
		return modelo;
	}

	public void setModelo(ColeccionVideos modelo) {
		this.modelo = modelo;
	}

	public VistaNuevoVideo getVista() {
		return vista;
	}

	public void setVista(VistaNuevoVideo vista) {
		this.vista = vista;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public File getImagen() {
		return imagen;
	}

	public void setImagen(File imagen) {
		this.imagen = imagen;
	}

}
