package modelo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class ColeccionVideos {
	private ArrayList<Video> videos = new ArrayList<Video>();
	private final String PATH = "assets/videos.csv";

	public ColeccionVideos() {
		leerArchivo();
	}

	public void agregar(Video video) {
		getVideos().add(video);
		actualizarArchivo();
	}

	public Boolean borrar(Video video) {
		if (getVideos().contains(video)) {
			getVideos().remove(video);
			actualizarArchivo();
			return true;
		}
		return false;

	}

	/*
	 * public void modificar() { }
	 */

	public ArrayList<Video> consultar() {
		return getVideos();
	}

	private void leerArchivo() {
		String[] datos;
		try (Scanner input = new Scanner(Paths.get(PATH))) {
			input.useDelimiter("\\r\\n|\\n\\r");
			while (input.hasNext()) {
				datos = input.next().split(";");
				getVideos().add(new Video(Integer.valueOf(datos[0]), datos[1], datos[2], datos[3]));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void actualizarArchivo() {
		try (Formatter output = new Formatter(PATH)) {
			for (Video video : getVideos()) {

				output.format("%d;%s;%s;%s%n", video.getId(), video.getTitulo(), video.getImagen(),
						video.getCategoria());

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public Integer getLastId() {
		return getVideos().size();
	}

	private ArrayList<Video> getVideos() {
		return videos;
	}

	private void setVideos(ArrayList<Video> videos) {
		this.videos = videos;
	}

}
