package modelo;

public class Video {
	private Integer id;
	private String titulo;
	private String imagen;
	private String categoria;
	
	public Video(Integer id, String titulo, String imagen, String categoria) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.imagen = imagen;
		this.categoria = categoria;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	

}
