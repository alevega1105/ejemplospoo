package controlador;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import vista.Vista;

public class Controlador implements MouseMotionListener, MouseListener {
	
	private Vista vista; 
	

	public Controlador() {
		vista = new Vista(this);
		vista.setVisible(true);
	}
	
	

	private void eventOutput(String evento, MouseEvent e) {
		getVista().getTextArea().append(evento
				+ " ("+ e.getX()+ ","+ e.getY()+")"
				+ "detectado en "
				+ e.getComponent().getClass().getName()
				+ "\n");
		getVista().getTextArea().setCaretPosition(getVista().getTextArea().getDocument().getLength());
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		eventOutput("Mouse Clicked (# de clicks: "+e.getClickCount()+")",e);
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		eventOutput("Mouse Pressed",e);
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		eventOutput("Mouse Released (# de clicks: "+e.getClickCount()+")",e);
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		eventOutput("Mouse Entered",e);
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		eventOutput("Mouse Exited",e);
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		eventOutput("Mouse Dragged",e);
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		eventOutput("Mouse Moved",e);
		
	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

}
