package primerEjemplo;

public class Resumen {

	public static void main(String[] args) {
		/*
		 * Explicación de: tipos primitivos, operadores lógicos, visibilidad y alcance
		 * de variables, imports, static, final, main, comentarios, if, if corto,
		 * switch, for, while, do while, String (sin profundizar en que es una clase),
		 * arreglos estáticos e imprimir por consola.
		 */
		
		
		// comentario de linea simple.
		/*
		 * este comentario es de multiples lineas.
		 */
		/**
		 * JAVA DOC
		 * 
		 * varias lineas.
		 */

		// TIPOS PRIMITIVOS
		int numEntero = 8;
		double numFlotante = 2.2;
		boolean booleano = true;
		char caracter = 'A';

		int casteo = (int) 3.4;
		System.out.println("numero: " + casteo);

		// OPERADORES LOGICOS
		if (numEntero == numFlotante) {
			System.out.println("los numeros son iguales");
		} else if (numEntero > numFlotante) {
			System.out.println("el entero es mayor que el flotante");
		} else {
			System.out.println("el entero es menor que el flotante");
		}
		/*
		 * igualdad: == desigual o distinto: != menor: < mayor: > menor igual: <= mayor
		 * igual; >=
		 * 
		 */

		if (numEntero != numFlotante && numEntero > 7) {
			System.out.println("el numero entero es distinto al flotante y es mayor a siete");
		}
		/*
		 * &: y |: o &&: y no evaluando la segunda parte de la condicion si la primera
		 * es falsa ||: o no evaluando la segunda parte de la condicion si la primera es
		 * verdadera ^: xor.
		 */
		System.out.println(numEntero > numFlotante ? "entero > flotante" : "entero <= flotante");

		int resultado = numEntero != 0 ? 100 / numEntero : 0;
		switch (caracter) {
		case 'A':

			System.out.println("valor en caracter es A ");
			break;
		case 'B':
			System.out.println("valor en caracter es B");
			break;

		default:
			throw new IllegalArgumentException("Unexpected value: " + caracter);
		}
		// BUCLES

		int[] numerosEnteros = { 1, 5, 19, 7, 3, 4, 9 };

		int i = 0;
		while (i < numerosEnteros.length) {
			System.out.println(numerosEnteros[i]);
			i = i + 1;
			// i++;
		}
		int j = 6;

		do {
			numerosEnteros[j] = j;
			System.out.println(numerosEnteros[j]);
			j--;
		} while (j >= 0);

		for (int k = 0; k < numerosEnteros.length; k++) {
			System.out.println("en for: " + numerosEnteros[k]);
		}

		// operadores de asignacion compuestos
		i += 7; // igual a i=i+7
		i -= 4;
		i *= 5;
		i /= 3;
		i %= 9;
		// ++ y --
		++i; // incrementa en uno y usa el valor
		i++; // usa el valor y despues incrementa

	}

}
