package primerEjemplo;
import java.security.SecureRandom;
import java.util.Random;

public class EjemploMath {

	public static void main(String[] args) {
		Math.pow(5, 2);
		Math.sqrt(900);
		Math.abs(23.7);
		Math.ceil(9.2);
		Math.floor(9.2);
		Math.cos(0.0);
		Math.sin(0.0);
		Math.tan(0.0);
		Math.log(Math.E);
		Math.max(2.3, 12.7);
		Math.min(2.3, 12.7);
		
		

		Random ran = new Random(); // produce valores deterministicos, que pueden ser predecidos.
		ran.nextInt();

		SecureRandom sran = new SecureRandom(); // produce numeros aleatorios no deterministicos
		System.out.println(sran.nextInt(2));

		String cadena = "este es un String";
		String cadena2 = new String("hola");

		for (int i = 0; i < cadena.length(); i++) {
			System.out.println(cadena.charAt(i));
		}

		for (char caracter : cadena.toCharArray()) {
			System.out.println(caracter);
		}

		System.out.println(cadena.substring(0, 4));
		

		if (cadena2 == "hola") { //cambiar por equals()
			System.out.println("con == son iguales");
		} else {
			System.out.println("con == no son iguales ");
		}

		if (cadena2.equalsIgnoreCase("HOLA")) {
			System.out.println("con equalsIgnoreCase son iguales");
		} else {
			System.out.println("no son iguales con equalsIgnoreCase");
		}

	}

}
