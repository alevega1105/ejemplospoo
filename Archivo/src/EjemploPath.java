import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class EjemploPath {

	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Ingrese nombre del archivo o directorio:");
		
		Path path = Paths.get(input.nextLine());
		
		if(Files.exists(path)) {
			System.out.printf("%n%s existe%n", path.getFileName());
			System.out.printf("%s un directorio%n", Files.isDirectory(path)? "Es":"No es");
			System.out.printf("%s es una direccion absoluta%n", path.isAbsolute() ? "Es": "No es");
			System.out.printf("ultima modificaci�n: %s%n", Files.getLastModifiedTime(path));
			System.out.printf("Tama�o %s%n", Files.size(path));
			System.out.printf("Direcci�n: %s%n", path);
			System.out.printf("Direcci�n absoluta: %s%n", path.toAbsolutePath());
			
			if(Files.isDirectory(path)) {
				System.out.printf("%nContenido:%n");
				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
				
				for (Path p : directoryStream) {
					System.out.println(p);
				}
			}else {
				System.out.printf("%s no existe%n", path);
			}
		}
	}

}
