import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Pattern;



public class EjemploLectura {

	public static void main(String[] args) {
		try(Scanner input = new Scanner(Paths.get("assets/probando.csv"))){
			System.out.printf("%-10s%-12s%-12s%10s%n", "cuenta", "nombre","apellido","balance");
			input.useDelimiter("\\r\\n|\\n\\r");
			String[] datos;
			while(input.hasNext()) {
				datos = input.next().split(";");
				System.out.println(Integer.valueOf(datos[0])+ " "+datos[1]+ " "+ datos[2]+" "+ Double.valueOf(datos[3].replace(",", ".")));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

/*
 * String[] datos;
		try(Scanner input = new Scanner(Paths.get("assets/probando.csv"))){
			System.out.printf("%-10s%-12s%-12s%10s%n", "cuenta", "nombre","apellido","balance");
			input.useDelimiter("\\r\\n|\\n\\r");
			while(input.hasNext()) {
				datos = input.next().split(";");
				System.out.println(Integer.valueOf(datos[0])+ " "+datos[1]+ " "+ datos[2]+" "+ Double.valueOf(datos[3].replace(",", ".")));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
 * */
