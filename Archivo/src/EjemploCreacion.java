import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class EjemploCreacion {

	public static void main(String[] args) {

		try (Formatter output = new Formatter("assets/probando.csv")) {
			Scanner input = new Scanner(System.in);
			System.out.printf("%s%n%s%n?", "Ingrese numero de cuenta, nombre, apellido, balance.",
					"ingrese ctrl+z para finalizar.");
			while (input.hasNext()) {
				try {
					output.format("%d;%s;%s;%.2f%n", input.nextInt(), input.next(), input.next(), input.nextDouble());
				} catch (NoSuchElementException elementException) {
					System.err.println("Valor invalido. Ingrese otra vez");
					input.nextLine();
				}
				System.out.print("? ");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
