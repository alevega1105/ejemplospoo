import java.util.Scanner;

public class PruebaAssert {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Escriba un numero entre 0 y 10: ");
		int numero = entrada.nextInt();
		
		assert (numero >=0 && numero<= 10) : "numero incorrecto " + numero;
		System.out.printf("Usted escribio %d%n", numero);

	}

}
