import java.util.InputMismatchException;
import java.util.Scanner;

public class EjemploExcepciones {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean continuarCiclo = true;
		do {
			try {
				System.out.println("Introduzca un numerador entero: ");
				int numerador = input.nextInt();
				System.out.println("Introduzca un denominador entero: ");
				int denominador = input.nextInt();

				int resultado = cociente(numerador, denominador);
				System.out.printf("%nResultado: %d / %d = %d%n", numerador, denominador, resultado);
				continuarCiclo = false;
			} /*catch (InputMismatchException inputMismatchException) {
				System.err.printf("%n Excepcion: %s%n", inputMismatchException);
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			} catch (ArithmeticException arithmeticException) {
				System.err.printf("%n Excepcion: %s%n", arithmeticException);
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			}*/
			
			catch (InputMismatchException| ArithmeticException e) {
				System.err.printf("%n Excepcion: %s%n", e);
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			}
			finally {
				System.out.println("Lo que esta en el bloque finally siempre se ejecuta");
			}
			
		} while (continuarCiclo);

	}

	public static int cociente(int numerador, int denominador) {
		return numerador / denominador;
	}

}
