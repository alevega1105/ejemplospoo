package controlador;

import java.awt.EventQueue;

import vista.VentanaColorDos;
import vista.VentanaColorUno;
import vista.VentanaOpcionColor;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaOpcionColor observado = new VentanaOpcionColor();
					observado.setVisible(true);
					new VentanaColorUno(observado).setVisible(true);
					new VentanaColorDos(observado).setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
