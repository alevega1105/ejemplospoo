package vista;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VentanaColorUno extends JFrame implements VentanaObserver {

	private JPanel contentPane;

	public VentanaColorUno(VentanaOpcionColor observado) {
		observado.attach(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	@Override
	public void actualizar(String color) {
		switch (color) {
		case "ROJO":
			contentPane.setBackground(Color.RED);
			break;
		case "AZUL":
			contentPane.setBackground(Color.BLUE);
			break;
		case "AMARILLO":
			contentPane.setBackground(Color.YELLOW);
			break;
		}
	}

}
