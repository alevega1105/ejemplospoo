package vista;

public interface VentanaObserver {
	
	public void actualizar(String color);

}
