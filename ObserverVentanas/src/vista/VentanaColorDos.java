package vista;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VentanaColorDos extends JFrame implements VentanaObserver {

	private JPanel contentPane;
	private JLabel lblColor;


	public VentanaColorDos(VentanaOpcionColor observado) {
		observado.attach(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Color seleccionado desde el patr\u00F3n");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(43, 61, 313, 31);
		contentPane.add(lblNewLabel);
		
		lblColor = new JLabel("ACA COLOR");
		lblColor.setBounds(161, 103, 93, 31);
		contentPane.add(lblColor);
	}

	@Override
	public void actualizar(String color) {
		getLblColor().setText(color);
		
	}

	public JLabel getLblColor() {
		return lblColor;
	}

	public void setLblColor(JLabel lblColor) {
		this.lblColor = lblColor;
	}

}
