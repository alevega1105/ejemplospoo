package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VentanaOpcionColor extends JFrame {

	private JPanel contentPane;
	private ArrayList<VentanaObserver> observers = new ArrayList<VentanaObserver>();
	private JComboBox comboBox;


	public void attach (VentanaObserver observer ) {
		getObservers().add(observer);
	}
	public void notifyAllObserver() {
		for (VentanaObserver ventanaObserver : observers) {
			ventanaObserver.actualizar(getComboBox().getSelectedItem().toString());
		}
	}

	public VentanaOpcionColor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				notifyAllObserver();
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"ROJO", "AZUL", "AMARILLO"}));
		comboBox.setBounds(132, 88, 155, 22);
		contentPane.add(comboBox);
		
		JLabel lblNewLabel = new JLabel("Patron Observer: Cambio de color sobre una vista");
		lblNewLabel.setBounds(98, 56, 248, 14);
		contentPane.add(lblNewLabel);
	}
	public ArrayList<VentanaObserver> getObservers() {
		return observers;
	}
	public void setObservers(ArrayList<VentanaObserver> observers) {
		this.observers = observers;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}
}
