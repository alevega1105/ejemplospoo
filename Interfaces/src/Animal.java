
public class Animal {

	private String nombre;
	
	public Animal(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String comer() {
		return "Come";
	}
	
	public String dormir() {
		return "Duerme";
	}
		
}
