
public class Perro extends Animal implements Mascota{

	private String raza;
	
	public Perro(String nombre, String raza) {
		super(nombre);
		this.setRaza(raza);
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String comer() {
		return super.comer() + " con el boca";
	}
	
	public String dormir() {
		return super.dormir() + " acostado";
	}

	@Override
	public String juega() {
		// TODO Auto-generated method stub
		return "Corre por la casa";
	}

	@Override
	public String acompa�ar() {
		// TODO Auto-generated method stub
		return "Se sienta al lado mio";
	}

	@Override
	public String subirSillon() {
		// TODO Auto-generated method stub
		return "Salta y se sienta";
	}
	
	
}
