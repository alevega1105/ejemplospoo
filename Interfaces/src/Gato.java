
public class Gato extends Animal implements Mascota{

	private String raza;

	public Gato(String nombre, String raza) {
		super(nombre);
		this.raza = raza;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	@Override
	public String juega() {
		// TODO Auto-generated method stub
		return "Araņandome la ropa";
	}

	@Override
	public String acompaņar() {
		// TODO Auto-generated method stub
		return "Se acuesta en mis piernas";
	}

	@Override
	public String subirSillon() {
		// TODO Auto-generated method stub
		return "salta y se enrolla";
	}
	
	
	
}
